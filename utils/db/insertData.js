const db = require('../../app/models');
const crypto = require('crypto');

const now = new Date();
const usuarios = [
  { 
    nombres: 'Tony',
    apellidos: 'Stark',
    email: 'imironman@avenger.org',
    dni: '11223344',
    password: '1234',
  },
  { 
    nombres: 'Bruce',
    apellidos: 'Banner',
    email: 'thehulk@avenger.org',
    dni: '11223355',
    password: '1234',
  },
  { 
    nombres: 'Steve',
    apellidos: 'Rogers',
    email: 'capi@avenger.org',
    dni: '11223366',
    password: '1234',
  },
  { 
    nombres: 'Thor',
    apellidos: 'Odinson',
    email: 'thor@avenger.org',
    dni: '11223377',
    password: '1234',
  },
];

const tareas = [
  {
    titulo: 'salvar el mundo',
    detalle: 'lo de siempre',
    fecha: now,
    usuarioId: 1,
  },
  {
    titulo: 'salvar el mundo otra vez',
    detalle: 'lo de siempre',
    fecha: now,
    usuarioId: 1,
  },
  {
    titulo: 'derrotar al mandarin',
    detalle: 'los 10 anillos',
    fecha: now,
    usuarioId: 1,
  },
  {
    titulo: 'no molestarce',
    detalle: 'keep calm and ...',
    fecha: now,
    usuarioId: 2,
  },
  {
    titulo: 'Derrotar a hodra',
    detalle: 'lo de siempre',
    fecha: now,
    usuarioId: 3,
  },
  {
    titulo: 'Pelear con los nazis',
    detalle: 'lo de siempre',
    fecha: now,
    usuarioId: 3,
  },
  {
    titulo: 'Loki again',
    detalle: 'mas brujeria',
    fecha: now,
    usuarioId: 4,
  },
  {
    titulo: 'Luchar contra hulk',
    detalle: 'quien es el mas fuerte?',
    fecha: now,
    usuarioId: 4,
  },
 
];

async function insertData() {
  console.log('Iniciando la insercion de tablas');
  console.log('-----------------------------');
  // usuarios
  for (const usuario of usuarios) {
    usuario.password = crypto.createHmac('sha256', 'llavedehash')
                               .update(usuario.password)
                               .digest('hex');
    await db.Usuario.create(usuario);
  }
  // tareas
  // for (const tarea of tareas) {
  //   await db.Tarea.create(tarea);
  // }
  console.log('-----------------------------');
  console.log('Insercion de tablas finalizado');
}

// ejecucion de la funcion
insertData();
