const express = require('express');
const router = express.Router();
const authInterceptor = require('../interceptors/auth.interceptor');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express', session: req.session });
});

router.get('/protegido', authInterceptor.isLogin, function(req, res, next) {
  res.render('index', { title: 'sitio protegido', session: req.session });
});

router.get('/desprotegido', function(req, res, next) {
  res.render('index', { title: 'sitio desprotegido', session: req.session });
});

router.get('/sin-acceso', function(req, res, next) {
  res.render('e401', {});
});



module.exports = router;
